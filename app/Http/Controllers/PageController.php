<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{

    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function blogIndex($blog = 'all') {
        $allPosts = $blog == 'all' ?
            $this->extractPostsFromMultipleBlogs(['kbs-news-events', 'music-matters', 'staff-student-news']) :
            $this->extractPosts($blog);
        $order = $this->request->query('order');
        $filters = $this->getFilters($allPosts[0]);
        $sortedPosts = $this->sortBlogPosts($allPosts, $order);

        // Paginate calculations
        $postsPerPage = intval($this->request->query('posts_per_page')) == 0 ? 10 : intval($this->request->query('posts_per_page'));
        $pageNumber = intval($this->request->query('page')) == 0 ? 1 : intval($this->request->query('page'));
        $lastPageNumber = intdiv((count($sortedPosts) - 1), $postsPerPage) + 1;
        $startPosition = ($pageNumber - 1) * $postsPerPage;
        $sortedPaginatedPosts = array_slice($sortedPosts, $startPosition, $postsPerPage);

        return view('page.index', [
            'posts' => $sortedPaginatedPosts,
            'blog' => $blog,
            'blogDisplayName' => $this->getBlogDisplayName($blog),
            'order' => $order,
            'pageNumber' => $pageNumber,
            'lastPageNumber' => $lastPageNumber,
            'postsPerPage' => $postsPerPage,
            'filters' => $filters
        ]);
    }

    public function show($blog, $id, $slug) {
        $post = json_decode(file_get_contents("https://api.kent.ac.uk/api/v1/blogs/{$blog}/{$id}", true));
        $post->blog_name = $blog;
        $post->blog_display_name = $this->getBlogDisplayName($blog);

        return view('page.show', ['post' => $post, 'blog' => $blog]);
    }

    // ------------ Private functions ------------ //

    // This function is used to get the current active filters' data nodes from
    // the filters' ids in the URL in order to display their names on the webpage.
    // It takes a post that went through the filters and loops through all its
    // categories, tags and author, picks the ones that matches the ids in the URL.
    private function getFilters($post) {
        $filters = ['author' => [], 'categories' => [], 'tags' => []];
        foreach ($this->request->query as $queryKey => $queryValue) {
            if ($queryKey == 'author') $filters['author'][] = $post->author;
            if ($queryKey == 'tags') {
                foreach ($post->tags as $tag) {
                    if (in_array($tag->id, explode(',', $queryValue))) {
                        $filters['tags'][] = $tag;
                    }
                }
            }
            if ($queryKey == 'categories') {
                foreach ($post->categories as $category) {
                    if (in_array($category->id, explode(',', $queryValue))) {
                        $filters['categories'][] = $category;
                    }
                }
            }
        }
        return $filters;
    }

    private function extractPostsFromMultipleBlogs($blogs) {
        $allPosts = [];
        foreach ($blogs as $blog) {
            $blogPosts = $this->extractPosts($blog);
            $allPosts = array_merge($allPosts, $blogPosts);
        }

        return $allPosts;
    }

    private function extractPosts($blog) {
        $blogData = json_decode(file_get_contents($this->queryToAPIRequest($blog), true));
        $blogPosts = $blogData->posts;
        $this->appendBlogNameToEachPost($blogPosts, $blog);

        return $blogPosts;
    }

    // I decided to use query, not path, to filter posts. This function transfer the query
    // in the URL to the data API path format.
    private function queryToAPIRequest($blog) {
        $queryString = "https://api.kent.ac.uk/api/v1/blogs/{$blog}";
        $possibleFilters = ['categories', 'tags', 'academics', 'schools', 'author'];
        foreach ($possibleFilters as $filter) {
            if ($this->request->query($filter)) {
                $queryString .= "/{$filter}/{$this->request->query($filter)}";
            }
        }

        return $queryString;
    }

    // In the data API, the name of the blog that a post belongs to is not included
    // in the post data. This function appends these two data nodes to the post data.
    private function appendBlogNameToEachPost($posts, $blogName) {
        foreach ($posts as $post) {
            $post->blog_name = $blogName;
            $post->blog_display_name = $this->getBlogDisplayName($blogName);
        }
    }

    private function getBlogDisplayName($blog) {
        switch ($blog) {
            case 'all':
            return 'All blogs';
            break;
            case 'kbs-news-events':
            return 'KBS News & Events';
            break;
            case 'music-matters':
            return 'Music Matters';
            break;
            case 'staff-student-news':
            return 'Staff/Student News';
            break;
            default:
            return 'Blog posts';
        }
    }

    private function sortBlogPosts($posts, $order) {
        $sortedPosts = array_sort($posts, function($item) {
            return $item->date;
        });
        return $order == 'asc' ? $sortedPosts : array_reverse($sortedPosts);
    }
}
