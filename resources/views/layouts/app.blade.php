<!DOCTYPE html>
<html lang="{{ config('app.locale')  }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') - Exercise</title>
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    </script>
    <script src="{{ mix('/js/app.js') }}"></script>
</head>
<body>
    @yield('content')

    @include('layouts._back_to_top')
</body>
<footer>
    Website created by Yijun Wang
</footer>
</html>
