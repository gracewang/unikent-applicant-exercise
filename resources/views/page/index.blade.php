@extends('layouts.app')

@section('title', 'Post list')

@section('content')
<div class="header">
    <div class="container header-container">
        <div class="page-title">
            @if ($blog == 'all')
                <a href="{{ route('posts') }}">{{ $blogDisplayName }}</a>
            @else
                <a href="{{ route('blog', ['blog' => $blog]) }}">{{ $blogDisplayName }}</a>
            @endif
        </div>
    </div>
</div>
<div class="main">
    <div class="container posts-container">
        @include('page._options')

        @include('page._paginate')
        @foreach ($posts as $post)
            @include('page._post_row')
        @endforeach
        @include('page._paginate')

    </div>
</div>
@endsection

@include('page._switch_n_sort')
