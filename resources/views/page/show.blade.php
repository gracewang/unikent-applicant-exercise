@extends('layouts.app')

@section('title', $post->title)

@section('content')
<div class="container post-container">

    <div class="blog-nav">
        <a href="{{ route('posts') }}">All blogs</a> // <a href="{{ route('blog', ['blog' => $post->blog_name]) }}">{{ $post->blog_display_name }}</a>
    </div>

    <h1>{{ $post->title }}</h1>

    <p>by <a href="{{ route('posts', ['author' => $post->author->id]) }}">{{ $post->author->display_name }}</a>, posted on {{ date('j F Y',strtotime($post->date)) }}</p>

    @if ($post->featured_image)
        <img src="{{ $post->featured_image->sizes->full->url }}" alt="{{ $post->featured_image->alt_text }}" title= "{{ $post->title }}" class="img-responsive">
    @endif

    <p><?php echo $post->content ?></p>

    <div class="related-links">
        <div class="categories-links">
            Categories:
            @foreach ($post->categories as $category)
                <span><a href="{{ route('blog', ['blog' => $blog, 'categories' => $category->id]) }}">{{ $category->name }}</a></span>
            @endforeach
        </div>
        <div class="tags-links">
            Tags:
            @if (count($post->tags) == 0)
                <span>No tags</span>
            @else
                @foreach ($post->tags as $tag)
                    <span><a href="{{ route('blog', ['blog' => $blog, 'tags' => $tag->id]) }}">{{ $tag->name }}</a></span>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection
