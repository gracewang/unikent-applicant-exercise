<div class="post-item">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-sm-push-9">
        @if ($post->featured_image)
            <a href="{{ route('post', ['blog' => $post->blog_name, 'id' => $post->id, 'slug' => $post->slug])  }}" title="{{ $post->title }}">
                <img src="{{ $post->featured_image->sizes->full->url }}" alt="{{ $post->featured_image->alt_text }}" class="img-responsive">
            </a>
        @else
            <img src="https://blogs.kent.ac.uk/staff-student-news/files/2016/04/Uok_Logo_RGB294-1.jpg" alt="University of Kent blog post" class="img-responsive">
        @endif
        </div>
        <div class="col-md-9 col-sm-9 col-sm-pull-3">
            <h4>
                <strong><a href="{{ route('post', ['blog' => $post->blog_name, 'id' => $post->id, 'slug' => $post->slug]) }}">{{ $post->title }}</a></strong>
            </h4>
            <p><?php echo $post->excerpt; ?></p>
        </div>
    </div>
    <div class="post-info">
        by
        @if ($blog == 'all')
            <a href="{{ route('posts', ['author' => $post->author->id]) }}">{{ $post->author->display_name }}</a>
            in <a href="{{ route('blog', ['blog' => $post->blog_name]) }}">{{ $post->blog_display_name }}</a>,
        @else
            <a href="{{ route('blog', ['author' => $post->author->id, 'blog' => $blog]) }}">{{ $post->author->display_name }}</a>,
        @endif
        posted on {{ date('j F Y',strtotime($post->date)) }}
    </div>
</div>
