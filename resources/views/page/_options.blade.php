<div class="options">
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <label>Sort: </label>
            <select id="post-order" name="post-order" onchange="sortPosts();">
                @foreach (['desc', 'asc'] as $postOrder)
                @if ($order == $postOrder)
                    <option value="{{ $postOrder }}" selected>Date ({{ strtoupper($postOrder) }})</option>
                @else
                    <option value="{{ $postOrder }}">Date ({{ strtoupper($postOrder) }})</option>
                @endif
                @endforeach
            </select>
        </div>
        <div class="col-md-6 col-sm-6">
            <label>From: </label>
            <select id="blog-selector" name="blog-selector" onchange="switchBlog();">
                @foreach (['all' => 'All blogs', 'kbs-news-events' => 'KBS News & Events', 'music-matters' => 'Music Matters', 'staff-student-news' => 'Staff/Student News'] as $blogNameKey => $blogNameValue)
                @if ($blog == $blogNameKey)
                    <option value="{{ $blogNameKey }}" selected>{{ $blogNameValue }}</option>
                @else
                    <option value="{{ $blogNameKey }}">{{ $blogNameValue }}</option>
                @endif
                @endforeach
            </select>
        </div>
    </div>
</div>

@if (count($filters['author']) >0 || count($filters['categories']) > 0 || count($filters['tags']) > 0)
<div class="options">
    <div class="row">
        <div class="col-md-12">
            <strong>Current filter(s):</strong><br>
            @if (count($filters['author']) > 0)
                Author:
                @foreach ($filters['author'] as $author)
                    <a href="{{ route('blog', ['author' => $author->id, 'blog' => $blog]) }}">{{ $author->display_name }}</a><br>
                @endforeach
            @endif

            @if (count($filters['categories']) > 0)
                Catetories:
                @foreach ($filters['categories'] as $category)
                    <a href="{{ route('blog', ['categories' => $category->id, 'blog' => $blog]) }}">{{ $category->name }}</a><br>
                @endforeach
            @endif

            @if (count($filters['tags']) > 0)
                Tags:
                @foreach ($filters['tags'] as $tag)
                    <a href="{{ route('blog', ['tags' => $tag->id, 'blog' => $blog]) }}">{{ $tag->name }}</a>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endif
