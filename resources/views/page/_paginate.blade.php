<nav>
    <ul class="pager">
    @if ($pageNumber == 1)
        <li class="previous disabled"><a><span>&larr;</span> Previous</a></li>
    @else
        <li class="previous" onclick="turnPage('back');"><a href="#"><span>&larr;</span> Previous</a></li>
    @endif

    @if ($pageNumber >= $lastPageNumber)
        <li class="next disabled"><a>Next <span>&rarr;</span></a></li>
    @else
        <li class="next" onclick="turnPage('forward');"><a href="#">Next <span>&rarr;</span></a></li>
    @endif
    </ul>
</nav>
