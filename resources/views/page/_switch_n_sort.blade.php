<script>
var switchBlog = function() {
    var blogSelector = document.getElementById("blog-selector");
    if (blogSelector.value === "all") {
        var location = "/";
    } else {
        var location = "/blog/" + blogSelector.value;
    }
    window.location = location;
};

// This function is not used at the moment.
// It may be useful in future development.
var findGetParameter = function(parameterName) {
    var result = null, tmp = [];
    location.search.substr(1)
    .split("&")
    .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
    return result;
};

// This function takes a query key, looks at the current query string,
// and returns a new query string with the taken key=value query pair removed.
var reconstructQueryWithout = function(keyToRemove) {
    var tmp = [];
    var outputQueryString = "?";
    var splitQueryStrings = location.search.substr(1).split("&");
    var keyLength = keyToRemove.length;
    var keyMatchString = keyToRemove + "=";
    splitQueryStrings.forEach(function (item) {
        if (item.substr(0, keyLength + 1) !== keyMatchString) {
            outputQueryString += item;
        }
    });
    return outputQueryString;
};

var sortPosts = function() {
    var postOrder = document.getElementById("post-order");
    var query = reconstructQueryWithout("order");
    var location = window.location.pathname + query;
    if (postOrder.value === 'asc' || postOrder.value === 'desc' ) {
        location = location + "&order=" + postOrder.value;
    }
    window.location = location;
};

var turnPage = function(direction) {
    var currentPageNumber = {{ $pageNumber }};
    if (direction === 'back') {
        var pageNumber = currentPageNumber - 1;
    } else if (direction === 'forward') {
        var pageNumber = currentPageNumber + 1;
    }
    var query = reconstructQueryWithout("page");
    var location = window.location.pathname + query + "&page=" + pageNumber;
    window.location = location;
};
</script>
