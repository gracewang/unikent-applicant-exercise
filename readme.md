This is a project in response to [the web developer applicant exercise of the University of Kent](https://github.com/unikent/applicant-exercise)

Laravel became my framework of choice because I like the MVC concept which separates logic and presentation well. I'd like to take this chance to demonstrate the outcome of my self-learning.

Here are some of my design decisions:

- Instead of using the pagination API from the data server, I rolled out my own. The major reason for this is that when sorting posts by date, I would like to sort all posts rather than just the posts on current page. This would help a user who looks for the earliest posts in the whole blog. So I pull off all posts, sort them, then paginate them. Although this scarifies page loading time it is the way I can see to make proper sorting from the API data.

- When filters are active, they will be displayed above the post list. Try: http://exercise.mdl.im/?author=47799&categories=128895 . This was quite tricky since getting back tags'/categories'/authors' names from their ids isn't straight forward in the API. A database backend would have been much easier. I am sure users would benefit from this friendly design.

- I use query (?author=47799&categories=128895) instead of path matching the API (/author/47799/caegories/128895) in the URI to filter posts. The reason is that query is more flexible and more fault-tolerant. Plus that it makes my route file shorter.

- The URL for a single post page does not match the API path (/blogs/@blog/@id) but includes post slug for SEO concern.

- For the posts that do not have featured images, a default image (University of Kent logo) is used for aesthetic reason.

Thank you very much.